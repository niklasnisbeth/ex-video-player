defmodule Ui.Router do
  use Ui.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    #plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :authorized do
    plug :check_cookie
  end

  scope "/", Ui do
    pipe_through :browser # Use the default browser stack

    post "/login", LoginController, :loginprocess
    get "/login", LoginController, :loginpage
    get "/logout", LoginController, :logout
    post "/logout", LoginController, :logout

    pipe_through :authorized

    get "/", PageController, :index

    get "/play", MediaController, :play
    get "/stop", MediaController, :stop_player
    get "/toggle-loop", MediaController, :toggle_loop

    get "/playlist/add/:name", PlaylistController, :add
    get "/playlist/remove/:id", PlaylistController, :remove

    get "/files/compact", LibraryController, :compact_files
    #get "/files/delete", LibraryController, :delete_files
    #get "/files/delete/:id", LibraryController, :delete_file

    post "/dl", DownloadController, :download
    post "/dl/file", DownloadController, :file
  end

  defp check_cookie(conn, _) do
    case conn.cookies["loggedin"] do
      nil -> 
        conn
        |> redirect(to: "/login")
        |> halt()
      token ->
        case Base.decode64(token) do
          :error -> conn |> redirect(to: "/login") |> halt()
          {:ok, decoded} -> case Phoenix.Token.verify(Ui.Endpoint, "natriumklorid", decoded) do
            {:ok, "admin"} -> conn
            {:ok, _} -> conn |> redirect(to: "/login") |> halt()
            {:error, :invalid} -> conn |> redirect(to: "/login") |> halt()
          end
        end
    end
  end
end
