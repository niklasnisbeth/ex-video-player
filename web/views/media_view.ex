defmodule Ui.MediaView do
  use Ui.Web, :view

  def action_icons(status) do
    x = %{ playing: "glyphicon-stop", notplaying: "glyphicon-play" }[status]
  end

  def action_urls(status) do 
    %{ playing: "/stop", notplaying: "/play" }[status]
  end

  def loop_icons(status) do
    %{ looping: "color: lightblue", notlooping: "" }[status]
  end
end
