defmodule Ui.PlaylistController do
  use Ui.Web, :controller

  def add(conn, %{"name" => name}) do
    {:ok} = Ui.Playlist.add(name)
    conn |> redirect(to: "/") |> halt()
  end

  def remove(conn, %{"id" => id}) do
    {:ok} = Ui.Playlist.remove(id)
    conn |> redirect(to: "/") |> halt()
  end
end
