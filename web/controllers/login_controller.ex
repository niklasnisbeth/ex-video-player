defmodule Ui.LoginController do
  use Ui.Web, :controller

  @password Application.get_env(:ui, Ui.Endpoint)[:password]

  def loginpage(conn, _) do 
    render(conn, "login.html")
  end

  def logout(conn, _) do 
    IO.inspect @password
    conn 
    |> delete_resp_cookie("loggedin")
    |> put_flash(:info, "Logged ud")
    |> redirect(to: "/") |> halt()
  end 

  def loginprocess(conn, %{"password" => @password}) do 
    token = Phoenix.Token.sign(Ui.Endpoint, "natriumklorid", "admin")
    conn 
    |> put_resp_cookie("loggedin", Base.encode64(token))
    |> put_flash(:info, "Logged ind")
    |> redirect(to: "/") |> halt()
  end 

  def loginprocess(conn, _) do
    conn
    |> put_flash(:error, "Forkert password")
    |> redirect(to: "/login") |> halt
  end
end
