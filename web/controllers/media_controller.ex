defmodule Ui.MediaController do
  use Ui.Web, :controller

  def play(conn, _params) do 
    case Ui.Player.play() do
      {:started, _pid} -> conn
        |> put_flash(:info, "Started")
        |> redirect(to: "/") |> halt()
      {:busy} -> conn
        |> put_flash(:error, "Already playing")
        |> redirect(to: "/") |> halt()
      {:error, err} -> conn
        |> put_flash(:error, "Err " <> err)
        |> redirect(to: "/") |> halt()
    end
  end

  def stop_player(conn, _params) do
    case Ui.Player.stop() do
      {:notplaying} -> conn |> redirect(to: "/") |> halt()
      {:stopped} -> conn 
        |> put_flash(:info, "Stopped")
        |> redirect(to: "/") 
        |> halt()
    end
  end

  def toggle_loop(conn, _params) do
    Ui.Player.toggle_loop()
    conn
    |> redirect(to: "/")
    |> halt()
  end
end
