defmodule Ui.LibraryController do
  use Ui.Web, :controller

  # TODO show ui for deleting single files
  def delete_files(conn, _params) do
    conn
  end

  # TODO actually delete a file
  def delete_file(conn, _params) do
    conn
  end

  # delete what's not on the playlist
  def compact_files(conn, _params) do
    {:ok, playlist} = Ui.Playlist.list
    Ui.Files.delete_others(playlist)
    conn
    |> put_flash(:info, "Fjernede ubrugte filer")
    |> redirect(to: "/")
    |> halt
  end 
end
