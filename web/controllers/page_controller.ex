defmodule Ui.PageController do
  use Ui.Web, :controller

  defp styleFilename(n) do
    {n, String.replace(n, "_", " ")}
  end

  defp prepareFiles(files) do
    Enum.map(files, fn n -> styleFilename(n) end)
  end

  defp preparePlaylist(list) do
    Enum.zip(0..Enum.count(list), Enum.map(list, fn n -> styleFilename(n) end))
  end

  def index(conn, _params) do
    {:ok, list} = Ui.Playlist.list
    {:ok, files} = Ui.Player.playable_files
    {:ok, downloads} = Ui.Download.list
    {playing, looping} = Ui.Player.status
    render(conn, "index.html", 
           playstatus: playing,
           loopstatus: looping,
           list: preparePlaylist(list), 
           files: prepareFiles(files), 
           downloads: downloads)
  end
end
