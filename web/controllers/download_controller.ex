defmodule Ui.DownloadController do
  use Ui.Web, :controller

  def download(conn, %{"url" => url} = _params) do 
    case Ui.Download.download(url) do
      {:ok, _url} -> conn
        |> put_flash(:info, "Startede download af #{url}")
        |> redirect(to: "/") |> halt()
      {:error, _url} -> if (url == "") do 
        conn
        |> put_flash(:error, "Indtast venligst en URL!")
        |> redirect(to: "/") |> halt()
      else
        conn
        |> put_flash(:error, "Download af #{url} mislykkedes")
        |> redirect(to: "/") |> halt()
      end
    end
  end

  def file(conn, %{"files" => uploads} = _params) do
    files = Enum.map(uploads, fn u -> %{:name => u.filename, :path => u.path} end)
    case Ui.Files.addMultiple(files) do
      {:ok, res} -> 
        GenEvent.notify(:update_event_manager, {:update, {:upload_done, res}})
        conn |> send_resp(200, "Successful") |> halt()
      {:error, res} -> conn |> send_resp(400, "Failed") |> halt()
    end
  end
end
