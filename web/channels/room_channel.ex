defmodule Ui.RoomChannel do
  use Phoenix.Channel

  def join("room:updates", _message, socket) do
    {:ok, socket}
  end
  def join("room:" <> _private_room_id, _params, _socket) do
    {:error, %{reason: "unauthorized"}}
  end
end

defmodule Ui.RoomChannel.Event do
  use GenEvent

  def handle_event({:update, _thing}, _) do
    Ui.Endpoint.broadcast!("room:updates", "update", %{})
    {:ok, nil}
  end

  def register_with_manager(pid) do
    GenEvent.add_handler(pid, __MODULE__, nil)
  end
end
