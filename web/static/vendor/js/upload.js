$(function () {
  'use strict';

  // TODO make less dependent on 
  function mkUlId(path) {
    return "upload-task";
  };
  function addUpload(path) { 
    var elem = Array.join([
      '<li class="distribute" ',
      ' id="', mkUlId(path), '">',
      '<span>', 
      path, 
      '<span class="progress-bar"></span>',
      '</span>',
      '<span class="glyphicon glyphicon-repeat spin"></span>',
      '</li>'
    ],"");
    $("#task-list").append(elem);
  };
  
  $('#fileupload').fileupload({
    url: "/dl/file",
    dataType: 'json',
    start: function(e, data) {
      addUpload("Upload")
    },
    fail: function(e, data) {
      $("#upload-task").remove();
    },
    done: function (e, data) {
      $("#upload-task").remove();
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css(
        'width',
        progress + '%'
      );
    }
  }).prop('disabled', !$.support.fileInput)
    .parent().addClass($.support.fileInput ? undefined : 'disabled'); 
});
