defmodule Ui.Files do

  @mediaroot Application.get_env(:ui, Ui.Mediaplayer)[:mediaroot]

  def list do
    case File.ls(@mediaroot) do
      {:ok, _files} = res -> res
      {:error, _} = res -> res
    end
  end

  def realpath(name) do 
    case list do 
      {:ok, files} -> if (Enum.find(files, fn x -> x == name end) ) do
        {:ok, @mediaroot <> "/#{name}"}
      else 
        {:error, :enoent}
      end
      {:error, _} = res -> res
    end
  end

  def add(path) do 
    name = Path.basename(path)
    File.copy(path, @mediaroot <> "/#{name}")
  end

  def add(path, name) do
    File.copy(path, @mediaroot <> "/#{name}") 
  end

  def addMultiple(files) do
    res = Enum.map(files, fn f -> 
      add(f.path, f.name) 
    end) 
    IO.inspect res
    if Enum.all?(res, fn x -> elem(x, 0) == :ok end) do
      {:ok, files}
    else 
      {:error, files}
    end
  end

  def delete(name) do
    File.rm(sanitize(name))
  end

  def delete_others(names) do
    {:ok, files} = list()
    Enum.each(files, fn f -> 
      if Enum.find(names, fn n -> f == n end) do
        nil
      else 
        File.rm(sanitize(f))
      end end)
  end

  # sørg for kun at gøre noget ved filer i mediaroot
  defp sanitize(n) do
    @mediaroot <> "/#{Path.basename(n)}"
  end
end
