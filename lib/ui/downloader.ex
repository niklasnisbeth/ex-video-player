defmodule Ui.Download do
  use GenServer

  @ytdlbin Application.get_env(:ui, Ui.Mediaplayer)[:ytdlbin]
  @stdlbin Application.get_env(:ui, Ui.Mediaplayer)[:stdlbin]
  @mediaroot Application.get_env(:ui, Ui.Mediaplayer)[:mediaroot]

  def start_link(state \\ %{}, opts \\ []) do
    GenServer.start_link(__MODULE__, state, opts)
  end

  def download(url) do
    GenServer.call(__MODULE__, {:dl, url})
  end

  def list() do
    GenServer.call(__MODULE__, {:list})
  end

  def handle_call({:list}, _from, s) do
    {:reply, {:ok, s}, s}
  end

  def handle_call({:dl, url}, _from, s) do
    %{authority: host} = URI.parse(url)
    cond do 
      host == nil -> 
        {:reply, {:error, s}, s}
      String.match?(host, ~r/.*youtube.*/) -> 
        p = start_ytdl(url)
        {:reply, {:ok, s}, Map.put(s, url, p)}
      String.match?(host, ~r/.*vimeo.*/) -> 
        p = start_ytdl(url)
        {:reply, {:ok, s}, Map.put(s, url, p)}
      String.match?(host, ~r/.*skoletube.*/) -> 
        p = start_wgetdl(url)
        {:reply, {:ok, s}, Map.put(s, url, p)}
      true -> 
        {:reply, {:error, s}, s}
    end
  end

  def handle_call({:ended, url}, _from, s) do
    GenEvent.notify(:update_event_manager, {:update, {:download_done, url}})
    {:noreply, Map.delete(s, url)}
  end 

  defp start_ytdl(url) do
    Porcelain.spawn(@ytdlbin,
                    ytdl_params(url),
                    out: {:send, spawn process_callback(url)})
  end

  defp start_wgetdl(url) do
    Porcelain.spawn_shell("cd " <> @mediaroot <> " && " <> @stdlbin <> " " <> url,
                    out: {:send, spawn process_callback(url)})
  end

  defp ytdl_params(url) do 
    [url,
     "--restrict-filenames",
     "-o",@mediaroot <> "/%(title)s.%(ext)s"]
  end

  defp process_callback(url) do
    fn -> 
      receive do
        {_pid, :result, _status} -> GenServer.call(__MODULE__, {:ended, url})
      end
    end
  end
end
