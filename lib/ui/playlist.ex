defmodule Ui.Playlist do
  use GenServer

  def start_link(state \\ [], opts \\ []) do
    GenServer.start_link(__MODULE__, {0, state}, opts)
  end

  def add(filename) do
    GenServer.call(__MODULE__, {:add, filename})
  end

  def next() do
    GenServer.call(__MODULE__, {:get_next})
  end

  def list() do
    GenServer.call(__MODULE__, {:list})
  end

  def remove(id) do
    GenServer.call(__MODULE__, {:remove, id})
  end

  def reset() do
    GenServer.call(__MODULE__, {:reset})
  end

  def handle_call({:add, file}, _from, {pos, list}) do
    # TODO tjek eksistens..
    {:reply, {:ok}, {pos, Enum.concat(list, [file])}}
  end

  def handle_call({:get_next}, _from, {pos, list}) do
    if (pos >= Enum.count(list)) do
      {:reply, {:overflow, Enum.at(list, 0)}, {1, list}}
    else 
      {:reply, {:ok, Enum.at(list, pos)}, {pos+1, list}}
    end
  end

  def handle_call({:reset}, _from, {_, list}) do
    {:reply, {:ok}, {0, list}}
  end

  def handle_call({:list}, _from, {_, items} = s) do
    {:reply, {:ok, items}, s}
  end

  def handle_call({:remove, id}, _from, {pos, list}) do
    # TODO numeriske id'er er ikke stabile
    {intid, _} = Integer.parse(id)
    {:reply, {:ok}, {pos, List.delete_at(list, intid)}}
  end
end
