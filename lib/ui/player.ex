defmodule Ui.Player do
  use GenServer

  @playerbin Application.get_env(:ui, Ui.Mediaplayer)[:playerbin]
  
  @filetypes [".mkv", ".mp4", ".mp3"]

  #### UTIL ####
  def playable_files do # TODO move to Ui.Files and update callers..
    case Ui.Files.list() do
      {:ok, files} ->
        {:ok, Enum.filter(files, fn f -> String.ends_with?(f, @filetypes) end)}
      {:error, _err} = res -> res
    end
  end

  defp play(file) do
    {:ok, path} = Ui.Files.realpath(file)
    Porcelain.spawn(@playerbin, 
      [path],
      out: {:send, spawn fn -> 
        receive do
          {pid, :result, _status} -> GenServer.call(__MODULE__, {:ended, pid})
        end
      end
      })
  end

  #### API ####

  def start_link(_, opts \\ []) do 
    GenServer.start_link(__MODULE__, %{pid: nil, looping: false}, opts)
  end

  def play() do
    GenServer.call(__MODULE__, {:play})
  end

  def stop() do
    GenServer.call(__MODULE__, {:stop})
  end

  def status() do
    GenServer.call(__MODULE__, {:status})
  end

  def toggle_loop() do
    GenServer.call(__MODULE__, {:toggle_loop})
  end

  #### API HANDLES ####

  def handle_call({:play}, _from, s) do #TODO check error
    if (s.pid != nil) do
      {:reply, {:busy}, s}
    else 
      Ui.Playlist.reset
      {_, name} = Ui.Playlist.next
      p = play(name)
      {:reply, {:started, p}, Map.put(s, :pid, p)}
    end
  end

  def handle_call({:ended, _pid}, _from, %{pid: :stopped} = s) do 
    {:noreply, Map.put(s, :pid, nil)}
  end

  def handle_call({:ended, _pid}, _from, %{looping: looping} = s) do 
    {next, name} = Ui.Playlist.next
    cond do
      (next == :ok) ->
        p = play(name)
        {:noreply, Map.put(s, :pid, p)}
      (next == :overflow and looping) ->
        p = play(name)
        {:noreply, Map.put(s, :pid, p)}
      true ->
        GenEvent.notify(:update_event_manager, {:update, {:playlist_done, nil}})
        {:noreply, Map.put(s, :pid, nil)}
    end
  end

  def handle_call({:stop}, _from, s) do
    if (s.pid != nil and s.pid != :stopped) do 
      Porcelain.Process.stop(s.pid)
      {:reply, {:stopped}, Map.put(s, :pid, :stopped)}
    else
      {:reply, {:notplaying}, s}
    end
  end 

  def handle_call({:toggle_loop}, _from, %{looping: looping} = s) do
    {:reply, {:ok}, Map.put(s, :looping, !looping)}
  end

  def handle_call({:status}, _from, s) do
    pl = (if (s.pid) do :playing else :notplaying end)
    lp = (if (s.looping) do :looping else :notlooping end)
    {:reply, {pl, lp}, s}
  end
end
