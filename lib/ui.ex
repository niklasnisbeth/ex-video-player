defmodule Ui do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    # Define workers and child supervisors to be supervised
    children = [
      # Start the endpoint when the application starts
      supervisor(Ui.Endpoint, []),
      # Start your own worker by calling: Ui.Worker.start_link(arg1, arg2, arg3)
      worker(Ui.Player, [nil, [name: Ui.Player]]),
      worker(Ui.Playlist, [[], [name: Ui.Playlist]]),
      worker(Ui.Download, [%{}, [name: Ui.Download]]),
      worker(GenEvent, [[name: :update_event_manager]])
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Ui.Supervisor]
    with {:ok, pid} <- Supervisor.start_link(children, opts),
      :ok <- Ui.RoomChannel.Event.register_with_manager(:update_event_manager),
      do: {:ok, pid}
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    Ui.Endpoint.config_change(changed, removed)
    :ok
  end

end
