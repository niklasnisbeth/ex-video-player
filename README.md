# Exmpl

Phoenix/Elixir-based media player with web-based UI.

Spawns mplayer and youtube-dl.

Makes marginally more sense if the UI is accessed from a different machine than the one it is running on.

To start app:

  * Checkout sources
  * Configure paths and variables
  * Install dependencies with `mix deps.get`
  * Start Phoenix endpoint with `mix phoenix.server` 
