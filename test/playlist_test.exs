defmodule Ui.PlaylistTest do
  use ExUnit.Case

  test "play list" do
    Ui.Playlist.add('hest')
    Ui.Playlist.add('ko')
    assert {:ok, 'hest'} = Ui.Playlist.next
    assert {:ok, 'ko'} = Ui.Playlist.next
    assert {:overflow, 'hest'} = Ui.Playlist.next
  end 
end
